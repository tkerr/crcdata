# CRCDATA LIBRARY #
CRC Calculation Modules.

## CRCDATA ##
Template class used for protection of critical data.

Provides a container for a data object and a CRC.  The crc is generated and checked upon writing and reading.

Also provides a built-in 8-bit CRC function for protecting the data.  The user can use the built-in function or provide their own.

## CRC8 ##
Table-driven CRC8 computation functions.

## CRC8_P ##
Table-driven CRC8 computation functions using Arduino program memory.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License
https://opensource.org/licenses/MIT

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

