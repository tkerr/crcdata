/*****************************************************************************
 * crcdata.h
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Classes used for protection of critical data.
 */
#ifndef _CRCDATA_H_
#define _CRCDATA_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stddef.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
// Uncomment the following if a custom CRC8 computation function is used.
// The function must be named crcdata_crc8 and match the crcdata_crc8()
// function prototype below.
//#define CRCDATA_CRC8_ALT

#define CRCDATA_OK   (0)  //!< Data value is good (CRC OK)
#define CRCDATA_ERR (-1)  //!< Data value is bad (CRC or index error)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @brief Compute CRC8 on an array of bytes. Used internally by crcdata classes.
 * @param crc_in Input CRC
 * @param ptr Pointer to data array
 * @param cnt Number of bytes in data array
 * @return CRC8 of data array.
 */ 
uint8_t crcdata_crc8(uint8_t crc_in, const uint8_t* ptr, uint8_t cnt);


/**
 * @class crcdata
 * @brief Template class used for protection of critical data values.
 */
template <class T>
class crcdata
{
public:
    crcdata();                    //!< Default constructor
    crcdata(T val);               //!< Construct from the protected data type
    crcdata& operator=(T val);    //!< Asign from protected data type
    
    /**
     * @brief Read the object from protected storage and check its CRC.
     * @param val Object to receive the protected data.
     * @return CRCDATA_OK if the CRC is successful, or CRCDATA_ERR if the CRC check failed.
     */
    int read(T& val);
    
    /**
     * @brief Write the object into protected storage and check its CRC.
     * @param val Object to write into protected storage.
     * @return CRCDATA_OK if the CRC is successful, or CRCDATA_ERR if the CRC check failed.
     */
    int write(T val);             //!< Write val into protected storage, return CRCDATA_OK / CRCDATA_ERR
    
    uint8_t crc() {return m_crc;} //!< Return the data object CRC value
private:
    T m_val;                      //!< The critical data value
    uint8_t m_crc;                //!< The critical data value CRC
    static uint8_t prvCrc8(T val) {return crcdata_crc8(0xFF, (const uint8_t*)&val, sizeof(val));}
};


/**************************************
 * crcdata<T>::crcdata
 **************************************/
template <class T>
crcdata<T>::crcdata()
{
    memset(&m_val, 0, sizeof(m_val));
    m_crc = prvCrc8(m_val);
}


/**************************************
 * crcdata<T>::crcdata
 **************************************/
template <class T>
crcdata<T>::crcdata(T val) :
    m_val(val)
{
    m_crc = prvCrc8(m_val);
}


/**************************************
 * crcdata<T>::operator=
 **************************************/
template <class T>
crcdata<T>& crcdata<T>::operator=(T val)
{
    m_val = val;
    m_crc = prvCrc8(m_val);
    return *this;
}


/**************************************
 * crcdata<T>::read
 **************************************/
template <class T>
int crcdata<T>::read(T& val)
{
    val = m_val;
    uint8_t crc = prvCrc8(val);
    if (crc != m_crc) return CRCDATA_ERR;
    return CRCDATA_OK;
}


/**************************************
 * crcdata<T>::write
 **************************************/
template <class T>
int crcdata<T>::write(T val)
{
    m_val = val;
    m_crc = prvCrc8(m_val);
    uint8_t crc = prvCrc8(val);
    if (crc != m_crc) return CRCDATA_ERR;
    return CRCDATA_OK;
}


#endif // _CRCDATA_H_
