/*****************************************************************************
 * crc8.h
 * Adapted from web page material from Maxim Integrated
 * Web site material Copyright (c) Maxim Integrated Products, Inc. 
 * This file authored by Thomas Kerr AB3GY and provided to the public domain
 ****************************************************************************/

/**
 * @file
 * @brief
 * Table-driven CRC8 computation.
 *
 * Reference: Maxim Integrated Application Note 27: "Understanding and Using 
 * "Cyclic Redundancy Checks with Maxim 1-Wire and iButton Products"
 *
 * URL: https://www.maximintegrated.com/en/design/technical-documents/app-notes/2/27.html
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef _CRC8_H_
#define _CRC8_H_

/******************************************************************************
 * Lint options.
 ******************************************************************************/


/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
  

/******************************************************************************
 * Public functions.
 ******************************************************************************/
 
#ifdef __cplusplus
    extern "C" {
#endif

/**
 * @brief Compute CRC8 on an array of bytes.
 * @param crc Input CRC
 * @param ptr Pointer to data array
 * @param cnt Number of bytes in data array
 * @return CRC8 of data array.
 */ 
uint8_t crc8(uint8_t crc, const uint8_t *ptr, uint8_t cnt);


/**
 * @brief Compute CRC8 on a single byte.
 * @param crc Input CRC
 * @param b Data byte for CRC computation.
 * @return CRC8 of input byte.
 */ 
uint8_t crc8_byte(uint8_t crc, uint8_t b);


#ifdef __cplusplus
    }
#endif

#endif // _CRC8_H_
