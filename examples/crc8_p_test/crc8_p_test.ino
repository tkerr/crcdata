/******************************************************************************
 * crc8_p_test.ino
 * Authored by Thomas Kerr AB3GY and provided to the public domain.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Test sketch for the crc8_P library function.
 *
 * The test vector and result can be found here:
 * Maxim Integrated Application Note 27: "Understanding and Using 
 * "Cyclic Redundancy Checks with Maxim 1-Wire and iButton Products"
 *
 * URL: https://www.maximintegrated.com/en/design/technical-documents/app-notes/2/27.html
 */
 
#include <stdint.h>
#include "Arduino.h"
#include "crc8_p.h"

#define NUM_BYTES 7

uint8_t test_vector[NUM_BYTES] = {0x02, 0x1C, 0xB8, 0x01, 0x00, 0x00, 0x00};
uint8_t crc_expected = 0xA2;


void setup()
{
    Serial.begin(9600);
    while (!Serial) {}
    Serial.println("crc8_P test sketch");
    
    // Test vector from Maxim website.
    Serial.println("\nTest 1");
    uint8_t crc = crc8_P(0x00, test_vector, NUM_BYTES);
    Serial.print("Actual CRC:   "); Serial.println(crc, HEX);
    Serial.print("Expected CRC: "); Serial.println(crc_expected, HEX);
    if (crc == crc_expected) Serial.println("Test pass");
    else Serial.println("Test fail");
    
    // CRC8 of a byte with itself should be zero.
    Serial.println("\nTest 2");
    crc = crc8_byte_P(crc_expected, crc);
    Serial.print("Actual CRC:   "); Serial.println(crc, HEX);
    Serial.println("Expected CRC: 0");
    if (crc == 0) Serial.println("Test pass");
    else Serial.println("Test fail");
}

void loop()
{   

}
